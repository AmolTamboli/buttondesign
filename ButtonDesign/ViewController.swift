//
//  ViewController.swift
//  ButtonDesign
//
//  Created by Amol Tamboli on 21/03/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var btnEnter: UIButton!
    @IBOutlet var btnSubmit: roundButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btnEnter.applyDesign()
    }


}
extension UIButton{
    func applyDesign(){
        self.backgroundColor = UIColor.darkGray
        self.layer.cornerRadius = self.frame.height/2
        self.setTitleColor(UIColor.green, for: .normal)
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 4
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
}

class roundButton: UIButton {
    override func didMoveToWindow() {
        self.backgroundColor = UIColor.darkGray
        self.layer.cornerRadius = self.frame.height/2
        self.setTitleColor(UIColor.red, for: .normal)
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 4
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
}

